package ui;

import apptemplate.AppTemplate;
import components.AppStyleArbiter;
import controller.BuzzwordController;
import controller.FileController;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Screen;
import javafx.stage.Stage;
import propertymanager.PropertyManager;

import java.io.IOException;

import static settings.AppPropertyType.*;

/**
 * This class provides the basic user interface for this application, including all the file controls, but it does not
 * include the workspace, which should be customizable and application dependent.
 *
 * @author Richard McKenna, Ritwik Banerjee, Thomas Biscardi
 */
public class AppGUI implements AppStyleArbiter {

    protected AppTemplate app;
    protected FileController fileController;   // to react to file-related controls
    protected Stage          primaryStage;     // the application window
    protected Scene          primaryScene;     // the scene graph
    protected BorderPane     appPane;          // the root node in the scene graph, to organize the containers
    protected VBox           toolbarPane;      // the top toolbar
    protected StackPane      sp;
    protected Button         newProfileButton; // button to create a new profile
    protected Button         loginButton;      // button to login to your profile
    protected Button         profileButton;    // button to view your profile
    protected Button         logoutButton;
    protected ComboBox       modeButton;       // combo box to select a game mode
    protected Button         startButton;      // button to start a game
    protected Button         homeButton;       // button to go back to the home screen
    protected String         applicationTitle; // the application title

    private int appSpecificWindowWidth;  // optional parameter for window width that can be set by the application
    private int appSpecificWindowHeight; // optional parameter for window height that can be set by the application
    
    /**
     * This constructor initializes the file toolbar for use.
     *
     * @param initPrimaryStage The window for this application.
     * @param initAppTitle     The title of this application, which
     *                         will appear in the window bar.
     * @param app              The app within this gui is used.
     */
    public AppGUI(Stage initPrimaryStage, String initAppTitle, AppTemplate app) throws IOException, InstantiationException {
        this(initPrimaryStage, initAppTitle, app, -1, -1);
        this.app = app;
    }

    public AppGUI(Stage primaryStage, String applicationTitle, AppTemplate appTemplate, int appSpecificWindowWidth, int appSpecificWindowHeight) throws IOException, InstantiationException {
        this.app = appTemplate;
        this.appSpecificWindowWidth = appSpecificWindowWidth;
        this.appSpecificWindowHeight = appSpecificWindowHeight;
        this.primaryStage = primaryStage;
        this.applicationTitle = applicationTitle;
        initializeToolbar();                    // initialize the top toolbar
        initializeToolbarHandlers(appTemplate); // set the toolbar button handlers
        initializeWindow();                     // start the app window (without the application-specific workspace)

    }

    public VBox getToolbarPane() { return toolbarPane; }

    public BorderPane getAppPane() { return appPane; }
    
    /**
     * Accessor method for getting this application's primary stage's,
     * scene.
     *
     * @return This application's window's scene.
     */
    public Scene getPrimaryScene() { return primaryScene; }
    
    /**
     * Accessor method for getting this application's window,
     * which is the primary stage within which the full GUI will be placed.
     *
     * @return This application's primary stage (i.e. window).
     */
    public Stage getWindow() { return primaryStage; }

    /****************************************************************************/
    /* BELOW ARE ALL THE PRIVATE HELPER METHODS WE USE FOR INITIALIZING OUR AppGUI */
    /****************************************************************************/
    
    /**
     * This function initializes all the buttons in the toolbar at the top of
     * the application window. These are related to file management.
     */
    private void initializeToolbar() throws IOException {
        PropertyManager propertyManager = PropertyManager.getManager();
        Insets insets = new Insets(80, 20, 0, 10);
        sp = new StackPane();
        Rectangle rect = new Rectangle(130, Screen.getPrimary().getVisualBounds().getHeight());
        rect.setFill(Color.LIGHTGREY);
        rect.setStroke(Color.LIGHTGREY);
        toolbarPane = new VBox(20);
        toolbarPane.setPadding(insets);
        newProfileButton = initializeChildButton(toolbarPane, propertyManager.getPropertyValue(NEW_PROFILE_BUTTON_LABEL), true);
        loginButton = initializeChildButton(toolbarPane, propertyManager.getPropertyValue(LOGIN_BUTTON_LABEL), true);
        modeButton = initializeChildComboBox(toolbarPane, SELECT_MODE_LABEL.toString(), false);
        startButton = initializeChildButton(toolbarPane, propertyManager.getPropertyValue(START_GAME_BUTTON_LABEL), false);
        logoutButton = initializeChildButton(toolbarPane, "Logout", false);
        profileButton = initializeChildButton(toolbarPane, "View Profile", false);
        homeButton = initializeChildButton(toolbarPane, "Home", false);
        sp.getChildren().addAll(rect, toolbarPane);
        sp.setAlignment(Pos.CENTER_LEFT);
    }

    private void initializeToolbarHandlers(AppTemplate app) throws InstantiationException {
        fileController = new BuzzwordController(app);

        newProfileButton.setOnAction(e -> {
            try {
                fileController.handleNewProfileRequest();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        });

        loginButton.setOnAction(e -> {
            try {
                fileController.handleLoginRequest();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        });

        logoutButton.setOnAction(e -> fileController.handleLogoutRequest());

        profileButton.setOnAction(e -> fileController.handleViewProfileRequest());

        startButton.setOnAction(e -> fileController.handleStartRequest());

        homeButton.setOnAction(e -> fileController.handleHomeRequest());


//        newButton.setOnAction(e -> fileController.handleNewRequest());
//        saveButton.setOnAction(e -> {
//            try {
//                fileController.handleSaveRequest();
//            } catch (IOException e1) {
//                e1.printStackTrace();
//                System.exit(1);
//            }
//        });
//        loadButton.setOnAction(e -> {
//            try {
//                fileController.handleLoadRequest();
//            } catch (IOException e1) {
//                e1.printStackTrace();
//            }
//        });
//        exitButton.setOnAction(e -> fileController.handleExitRequest());
    }

    public String getModeSelection() {
        return (String) modeButton.getValue();
    }

    private void reinitHandlers() {
        newProfileButton.setOnAction(e -> {
            try {
                fileController.handleNewProfileRequest();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        });

        loginButton.setOnAction(e -> {
            try {
                fileController.handleLoginRequest();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        });

        logoutButton.setOnAction(e -> fileController.handleLogoutRequest());

        profileButton.setOnAction(e -> fileController.handleViewProfileRequest());

        startButton.setOnAction(e -> fileController.handleStartRequest());

        homeButton.setOnAction(e -> fileController.handleHomeRequest());
    }

    public void updateWorkspaceToolbar(int state){
        PropertyManager propertyManager = PropertyManager.getManager();
        switch (state) {
            case 0:
                toolbarPane.getChildren().clear();
                try {
                    newProfileButton = initializeChildButton(toolbarPane, propertyManager.getPropertyValue(NEW_PROFILE_BUTTON_LABEL), true);
                    loginButton = initializeChildButton(toolbarPane, propertyManager.getPropertyValue(LOGIN_BUTTON_LABEL), true);
                } catch (IOException e) {

                }
                break;
            case 1:
                toolbarPane.getChildren().clear();

                try {
                    newProfileButton = initializeChildButton(toolbarPane, propertyManager.getPropertyValue(NEW_PROFILE_BUTTON_LABEL), false);
                    profileButton = initializeChildButton(toolbarPane, "View Profile", true);
                    modeButton = initializeChildComboBox(toolbarPane, SELECT_MODE_LABEL.toString(), true);
                    startButton = initializeChildButton(toolbarPane, propertyManager.getPropertyValue(START_GAME_BUTTON_LABEL), true);
                    logoutButton = initializeChildButton(toolbarPane, /*app.getWorkspaceComponent().getBuzzwordState().getUser().getUsername()*/ "Logout", true);
                } catch (IOException e) {

                }
                break;
            case 2:
                toolbarPane.getChildren().clear();
                try {
                    newProfileButton = initializeChildButton(toolbarPane, propertyManager.getPropertyValue(NEW_PROFILE_BUTTON_LABEL), false);
                    profileButton = initializeChildButton(toolbarPane, "View Profile", true);
                    homeButton = initializeChildButton(toolbarPane, "Home", true);
//                    logoutButton = initializeChildButton(toolbarPane, /*app.getWorkspaceComponent().getBuzzwordState().getUser().getUsername()*/ "Logout", true);
                } catch (IOException e) {

                }
                break;
            case 3:
                toolbarPane.getChildren().clear();
                try {
                    newProfileButton = initializeChildButton(toolbarPane, propertyManager.getPropertyValue(NEW_PROFILE_BUTTON_LABEL), false);
                    logoutButton = initializeChildButton(toolbarPane, /*app.getWorkspaceComponent().getBuzzwordState().getUser().getUsername()*/ "Logout", true);
                    homeButton = initializeChildButton(toolbarPane, "Home", true);
//                    logoutButton = initializeChildButton(toolbarPane, /*app.getWorkspaceComponent().getBuzzwordState().getUser().getUsername()*/ "Logout", true);
                } catch (IOException e) {

                }
                break;
            default:
                break;
        }
        reinitHandlers();
    }

    // INITIALIZE THE WINDOW (i.e. STAGE) PUTTING ALL THE CONTROLS
    // THERE EXCEPT THE WORKSPACE, WHICH WILL BE ADDED THE FIRST
    // TIME A NEW Page IS CREATED OR LOADED
    private void initializeWindow() throws IOException {
        PropertyManager propertyManager = PropertyManager.getManager();

        // SET THE WINDOW TITLE
        primaryStage.setTitle(applicationTitle);
//        primaryStage.initStyle(StageStyle.UNDECORATED);

        // GET THE SIZE OF THE SCREEN
        Screen      screen = Screen.getPrimary();
        Rectangle2D bounds = screen.getVisualBounds();

        // AND USE IT TO SIZE THE WINDOW
//        primaryStage.setX(bounds.getMinX());
//        primaryStage.setY(bounds.getMinY());
        if(primaryStage.getWidth() == -1) {
            primaryStage.setWidth(bounds.getWidth());
            primaryStage.setHeight(bounds.getHeight());
        }

        // ADD THE TOOLBAR ONLY, NOTE THAT THE WORKSPACE
        // HAS BEEN CONSTRUCTED, BUT WON'T BE ADDED UNTIL
        // THE USER STARTS EDITING A COURSE
        appPane = new BorderPane();
        appPane.setStyle("-fx-background-color: darkgrey");
        appPane.setLeft(sp);
        primaryScene = appSpecificWindowWidth < 1 || appSpecificWindowHeight < 1 ? new Scene(appPane)
                                                                                 : new Scene(appPane,
                                                                                             appSpecificWindowWidth,
                                                                                             appSpecificWindowHeight);

//        URL imgDirURL = AppTemplate.class.getClassLoader().getResource(APP_IMAGEDIR_PATH.getParameter());
//        if (imgDirURL == null)
//            throw new FileNotFoundException("Image resrouces folder does not exist.");
//        try (InputStream appLogoStream = Files.newInputStream(Paths.get(imgDirURL.toURI()).resolve(propertyManager.getPropertyValue(APP_LOGO)))) {
//            primaryStage.getIcons().add(new Image(appLogoStream));
//        } catch (URISyntaxException e) {
//            e.printStackTrace();
//        }

        primaryStage.setScene(primaryScene);
        primaryStage.show();
    }
    
    /**
     * This is a public helper method for initializing a simple button with
     * an icon and tooltip and placing it into a toolbar.
     *
     * @param toolbarPane Toolbar pane into which to place this button.
     * @param visible    true if the button is to start off visible, false otherwise.
     * @return A constructed, fully initialized button placed into its appropriate
     * pane container.
     */
    public Button initializeChildButton(Pane toolbarPane, String label ,boolean visible) throws IOException {


        Button button = new Button(label);
        button.setAlignment(Pos.CENTER_LEFT);
        button.setVisible(visible);
        button.setPrefSize(150, 40);
        button.setStyle("-fx-background-color: grey; -fx-text-fill: white;");

        toolbarPane.getChildren().add(button);

        return button;
    }

    public ComboBox initializeChildComboBox(Pane toolbarPane, String label, boolean visible) {
        PropertyManager propertyManager = PropertyManager.getManager();

        ComboBox cb = new ComboBox();
        cb.setPromptText(propertyManager.getPropertyValue(label));
        cb.getItems().addAll(
                propertyManager.getPropertyValue(ENGLISH_DICTIONARY_LABEL),
                propertyManager.getPropertyValue(NOUNS_LABEL),
                propertyManager.getPropertyValue(ADJECTIVES_LABEL)
        );

        cb.setStyle("-fx-background-color: grey; -fx-prompt-text-fill: white;");

        cb.setVisible(visible);
        cb.setPrefSize(150, 40);

        toolbarPane.getChildren().add(cb);


        return cb;
    }
    
    /**
     * This function specifies the CSS style classes for the controls managed
     * by this framework.
     */
    @Override
    public void initStyle() {
        // currently, we do not provide any stylization at the framework-level
    }

    public void update(FileController fc) {
        fileController = fc;
    }

    public FileController getFileController() {
        return fileController;
    }

}
