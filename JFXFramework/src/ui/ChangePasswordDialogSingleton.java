package ui;


import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class ChangePasswordDialogSingleton extends Stage {


    static ChangePasswordDialogSingleton singleton = null;
    VBox    root;
    GridPane    dataArea;
    HBox    buttons;
    Label   oldPassword, password, title, confirm;
    PasswordField oldPasswordField, passwordField, confirmField;
    Button  login, cancel;
    String selection;
    Scene profileScene;

    public static final String LOGIN = "Okay";
    public static final String CANCEL = "Cancel";

    private ChangePasswordDialogSingleton() {}

    public static ChangePasswordDialogSingleton getSingleton() {
        if (singleton == null)
            singleton = new ChangePasswordDialogSingleton();
        return singleton;
    }

    public void init(Stage owner) {
        initModality(Modality.WINDOW_MODAL);
        initOwner(owner);

        title = new Label("Change Password");
        title.setTextFill(Color.WHITE);
        title.setAlignment(Pos.CENTER);
        title.setFont(new Font(20));

        oldPassword = new Label("Old Password: ");
        oldPassword.setAlignment(Pos.CENTER_LEFT);
        oldPassword.setTextFill(Color.WHITE);
        password = new Label("New Password: ");
        password.setAlignment(Pos.CENTER_LEFT);
        password.setTextFill(Color.WHITE);
        confirm = new Label("Confirm Password: ");
        confirm.setAlignment(Pos.CENTER_LEFT);
        confirm.setTextFill(Color.WHITE);

        oldPasswordField = new PasswordField();
        oldPasswordField.setPromptText("Old Password");
//        usernameField.setAlignment(Pos.CENTER);
        passwordField = new PasswordField();
        passwordField.setPromptText("New Password");
        confirmField = new PasswordField();
        confirmField.setPromptText("Confirm Password");
//        password.setAlignment(Pos.CENTER);

        dataArea = new GridPane();
        dataArea.setVgap(10);
        dataArea.setHgap(10);
        GridPane.setConstraints(oldPassword, 0, 0);
        GridPane.setConstraints(password, 0, 1);
        GridPane.setConstraints(confirm, 0, 2);
        GridPane.setConstraints(oldPasswordField, 1, 0);
        GridPane.setConstraints(passwordField, 1, 1);
        GridPane.setConstraints(confirmField, 1, 2);
        dataArea.getChildren().addAll(oldPassword, password, confirm, oldPasswordField, passwordField, confirmField);

        buttons = new HBox(20);
        login = new Button(LOGIN);
        login.setDefaultButton(true);
        cancel = new Button(CANCEL);
        buttons.setAlignment(Pos.CENTER);

        EventHandler<ActionEvent> loginProfileHandler = event -> {
            ChangePasswordDialogSingleton.this.selection = ((Button) event.getSource()).getText();
            ChangePasswordDialogSingleton.this.hide();
        };

        login.setOnAction(loginProfileHandler);
        cancel.setOnAction(loginProfileHandler);


        buttons.getChildren().addAll(login, cancel);

        root = new VBox(10);
        root.setStyle("-fx-background-color: black;");
        root.setAlignment(Pos.CENTER);
        root.getChildren().addAll(title, dataArea, buttons);
        root.setPadding(new Insets(10,10,10,10));

        this.initStyle(StageStyle.UNDECORATED);
        profileScene = new Scene(root);
        this.setScene(profileScene);
        this.setOpacity(.8);
    }

    public void show(String title) {
        setTitle(title);
        showAndWait();
    }

    public String getSelection() { return selection; }

    public TextField getOldPasswordField() { return oldPasswordField; }

    public PasswordField getPasswordField() { return passwordField; }

    public PasswordField getConfirmField() {
        return confirmField;
    }

    public void resetSelection() {
        oldPasswordField.clear();
        passwordField.clear();
        confirmField.clear();
        ChangePasswordDialogSingleton.this.selection = null;
    }

}
