package ui;


import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class CreateProfileDialogSingleton extends Stage {


    static CreateProfileDialogSingleton singleton = null;
    VBox    root;
    GridPane    dataArea;
    HBox    buttons;
    Label   username, password, title, confirm;
    TextField   usernameField;
    PasswordField passwordField, confirmField;
    Button  login, cancel;
    String selection;
    Scene profileScene;

    public static final String LOGIN = "Okay";
    public static final String CANCEL = "Cancel";

    private CreateProfileDialogSingleton() {}

    public static CreateProfileDialogSingleton getSingleton() {
        if (singleton == null)
            singleton = new CreateProfileDialogSingleton();
        return singleton;
    }

    public void init(Stage owner) {
        initModality(Modality.WINDOW_MODAL);
        initOwner(owner);

        title = new Label("Create New Profile");
        title.setTextFill(Color.WHITE);
        title.setAlignment(Pos.CENTER);
        title.setFont(new Font(20));

        username = new Label("Username: ");
        username.setAlignment(Pos.CENTER_LEFT);
        username.setTextFill(Color.WHITE);
        password = new Label("Password: ");
        password.setAlignment(Pos.CENTER_LEFT);
        password.setTextFill(Color.WHITE);
        confirm = new Label("Confirm Password: ");
        confirm.setAlignment(Pos.CENTER_LEFT);
        confirm.setTextFill(Color.WHITE);

        usernameField = new TextField();
        usernameField.setPromptText("Username");
//        usernameField.setAlignment(Pos.CENTER);
        passwordField = new PasswordField();
        passwordField.setPromptText("Password");
        confirmField = new PasswordField();
        confirmField.setPromptText("Confirm Password");
//        password.setAlignment(Pos.CENTER);

        dataArea = new GridPane();
        dataArea.setVgap(10);
        dataArea.setHgap(10);
        GridPane.setConstraints(username, 0, 0);
        GridPane.setConstraints(password, 0, 1);
        GridPane.setConstraints(confirm, 0, 2);
        GridPane.setConstraints(usernameField, 1, 0);
        GridPane.setConstraints(passwordField, 1, 1);
        GridPane.setConstraints(confirmField, 1, 2);
        dataArea.getChildren().addAll(username, password, confirm, usernameField, passwordField, confirmField);

        buttons = new HBox(20);
        login = new Button(LOGIN);
        login.setDefaultButton(true);
        cancel = new Button(CANCEL);
        buttons.setAlignment(Pos.CENTER);

        EventHandler<ActionEvent> loginProfileHandler = event -> {
            CreateProfileDialogSingleton.this.selection = ((Button) event.getSource()).getText();
            CreateProfileDialogSingleton.this.hide();
        };

        login.setOnAction(loginProfileHandler);
        cancel.setOnAction(loginProfileHandler);


        buttons.getChildren().addAll(login, cancel);

        root = new VBox(10);
        root.setStyle("-fx-background-color: black;");
        root.setAlignment(Pos.CENTER);
        root.getChildren().addAll(title, dataArea, buttons);
        root.setPadding(new Insets(10,10,10,10));

        this.initStyle(StageStyle.UNDECORATED);
        profileScene = new Scene(root);
        this.setScene(profileScene);
        this.setOpacity(.8);
    }

    public void show(String title) {
        setTitle(title);
        showAndWait();
    }

    public String getSelection() { return selection; }

    public TextField getUsernameField() { return usernameField; }

    public PasswordField getPasswordField() { return passwordField; }

    public PasswordField getConfirmField() {
        return confirmField;
    }

    public void resetSelection() {
        usernameField.clear();
        passwordField.clear();
        confirmField.clear();
        CreateProfileDialogSingleton.this.selection = null;
    }

}
