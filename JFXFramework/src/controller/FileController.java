package controller;

import java.io.IOException;

/**
 * @author Ritwik Banerjee
 */
public interface FileController {

    void handleNewProfileRequest() throws IOException;

    void handleLoginRequest() throws IOException;

    void handleStartRequest();

    void handleLogoutRequest();

    void handleHomeRequest();

    void handleViewProfileRequest();
}
