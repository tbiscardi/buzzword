package settings;

/**
 * This enum provides properties that are to be loaded via
 * XML files to be used for setting up the application.
 *
 * @author Richard McKenna, Ritwik Banerjee, Thomas Biscardi
 * @version 1.0
 */
@SuppressWarnings("unused")
public enum AppPropertyType {

    // from app-properties.xml
    APP_WINDOW_WIDTH,
    APP_WINDOW_HEIGHT,
    APP_TITLE,
    APP_LOGO,
    APP_CSS,
    APP_PATH_CSS,

    // TOOLBAR LABELS
    NEW_PROFILE_BUTTON_LABEL,
    LOGIN_BUTTON_LABEL,
    HOME_BUTTON_LABEL,
    SELECT_MODE_LABEL,
    ENGLISH_DICTIONARY_LABEL,
    NOUNS_LABEL,
    ADJECTIVES_LABEL,
    START_GAME_BUTTON_LABEL,
    HEADING_LABEL,

    // ERROR MESSAGES
    NEW_ERROR_MESSAGE,
    SAVE_ERROR_MESSAGE,
    LOAD_ERROR_MESSAGE,
    PROPERTIES_LOAD_ERROR_MESSAGE,
    SAVE_WRONG_EXT_MESSAGE,

    // ERROR TITLES
    NEW_ERROR_TITLE,
    SAVE_ERROR_TITLE,
    LOAD_ERROR_TITLE,
    PROPERTIES_LOAD_ERROR_TITLE,
    SAVE_WRONG_EXT_TITLE,

    // AND VERIFICATION MESSAGES AND TITLES
    NEW_COMPLETED_MESSAGE,
    NEW_COMPLETED_TITLE,
    SAVE_COMPLETED_MESSAGE,
    SAVE_COMPLETED_TITLE,
    SAVE_UNSAVED_WORK_TITLE,
    SAVE_UNSAVED_WORK_MESSAGE,

    SAVE_WORK_TITLE,
    WORK_FILE_EXT,
    WORK_FILE_EXT_2,
    WORK_FILE_EXT_DESC,
    RESULTS_TITLE,
    SUCCESS_MESSAGE,
    FAILURE_MESSAGE,
    PROPERTIES_
}
