package data;


public class Word {

    private String word;
    private int score;

    public Word(String word, int score) {
        this.word = word;
        this.score = score;
    }

    public Word(String word) {
        this.word = word;
        this.score = calculateScore(word);
    }


    public String getWord() {
        return word;
    }

    public int getScore() {
        return score;
    }

    private int calculateScore(String word) {
        if(word.length() < 3) {
            return 0;
        }
        if(word.length() == 3) {
            return 10;
        }
        if(word.length() == 4) {
            return 20;
        }
        if(word.length() == 5) {
            return 30;
        }
        if(word.length() == 6) {
            return 40;
        }
        return 50;
    }
}
