package data;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.awt.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.*;
import java.util.List;

public class BuzzwordSolver {

    private static NavigableSet<String> dictionary;
    private final Map<Character, List<Character>> graph = new HashMap<>();
    private List<String> list;
    private ObservableList<Word> fullList;

    public BuzzwordSolver(char[][] letters, NavigableSet<String> dictionary) {
        this.dictionary = dictionary;
        list = buzzwordSolver(letters);
        ArrayList<String> temp = new ArrayList<>();
        fullList = FXCollections.observableArrayList();
        for(String s: list) {
            if(!temp.contains(s) && s.length() >= 3) {
                temp.add(s);
                fullList.addAll(new Word(s));
            }
        }
    }

    public ObservableList<Word> getList() {
        return fullList;
    }

    public int getTotalScore() {
        int total = 0;
        for(int i = 0; i < fullList.size(); i ++) {
            total = total + fullList.get(i).getScore();
        }
        return total;
    }


    public List<String> buzzwordSolver(char[][] m) {
        if (m == null) {
            throw new NullPointerException("The matrix cannot be null");
        }

        final List<String> validWords = new ArrayList<>();
        for (int i = 0; i < m.length; i++) {
            for (int j = 0; j < m[0].length; j++) {
                ArrayList<Integer> arrX = new ArrayList<>();
                arrX.add(i);
                ArrayList<Integer> arrY = new ArrayList<>();
                arrY.add(j);
                solve(m, i, j, m[i][j] + "", validWords, arrX, arrY);
            }
        }
        return validWords;
    }

    private void solve(char[][] m, int i, int j, String prefix, List<String> validWords, ArrayList<Integer> arrX, ArrayList<Integer> arrY) {
        assert m != null;
        assert validWords != null;

        for (int i1 = Math.max(0, i - 1); i1 < Math.min(m.length, i + 2); i1++) {
            for (int j1 = Math.max(0, j - 1); j1 < Math.min(m[0].length, j + 2); j1++) {
                boolean b = false;
                for(int z = 0; z < arrX.size(); z++) {
                    if(arrX.get(z) == i1 && arrY.get(z) == j1) {
                        b = true;
                        break;
                    }
                }
                if ((i1 == i && j1 == j) || b) continue;

                String word = prefix+ m[i1][j1];
                arrX.add(i1);
                arrY.add(j1);
//                System.out.println(word);
                if (!dictionary.subSet(word, word + Character.MAX_VALUE).isEmpty()) {
                    if (dictionary.contains(word)) {
                        validWords.add(word);
//                        System.out.println("!!" + word + "!!");
                    }
                    solve(m, i1, j1, word, validWords, arrX, arrY);
                }
                arrX.remove(arrX.size()-1);
                arrY.remove(arrY.size()-1);
            }

        }
    }
}
