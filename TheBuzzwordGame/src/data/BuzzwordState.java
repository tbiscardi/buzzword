package data;

public class BuzzwordState {

    private int state;
    private Player user;

    public BuzzwordState(int state) {
        this.state = state;
    }

    public BuzzwordState(int state, Player user) {
        this.state = state;
        this.user = user;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public void setState(int state, Player user) {
        this.state = state;
        this.user = user;
    }

    public Player getUser() {
        return user;
    }

    public void setUser(Player user) {
        this.user = user;
    }

}
