package data;

public class Player {

    private String username;
    private String password;
    private int[] levelsCompleted;
    private int[] engHighScores, nounHighScores, adjHighScores;

    public int[] getEngHighScores() {
        return engHighScores;
    }

    public void setEngHighScores(int[] engHighScores) {
        this.engHighScores = engHighScores;
        if(engHighScores[0] == 0) {
            levelsCompleted[0] = 0;
        } else if(engHighScores[1] == 0) {
            levelsCompleted[0] = 1;
        } else if(engHighScores[2] == 0) {
            levelsCompleted[0] = 2;
        } else if(engHighScores[3] == 0) {
            levelsCompleted[0] = 3;
        } else {
            levelsCompleted[0] = 4;
        }
    }

    public int[] getNounHighScores() {
        return nounHighScores;
    }

    public void setNounHighScores(int[] nounHighScores) {
        this.nounHighScores = nounHighScores;
        if(nounHighScores[0] == 0) {
            levelsCompleted[1] = 0;
        } else if(nounHighScores[1] == 0) {
            levelsCompleted[1] = 1;
        } else if(nounHighScores[2] == 0) {
            levelsCompleted[1] = 2;
        } else if(nounHighScores[3] == 0) {
            levelsCompleted[1] = 3;
        } else {
            levelsCompleted[1] = 4;
        }
    }

    public int[] getAdjHighScores() {
        return adjHighScores;
    }

    public void setAdjHighScores(int[] adjHighScores) {
        this.adjHighScores = adjHighScores;
        if(adjHighScores[0] == 0) {
            levelsCompleted[2] = 0;
        } else if(adjHighScores[1] == 0) {
            levelsCompleted[2] = 1;
        } else if(adjHighScores[2] == 0) {
            levelsCompleted[2] = 2;
        } else if(adjHighScores[3] == 0) {
            levelsCompleted[2] = 3;
        } else {
            levelsCompleted[2] = 4;
        }
    }

    public Player() {
        levelsCompleted = new int[3];
    }

    public Player(String username, String password, int[] levelsCompleted) {
        this.username = username;
        this.password = password;
        this.levelsCompleted = levelsCompleted;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int[] getLevelsCompleted() {
        return levelsCompleted;
    }

    public void setLevelsCompleted(int[] levelsCompleted) {
        this.levelsCompleted = levelsCompleted;
    }

    public int getEnglishCompleted() {
        return levelsCompleted[0];
    }

    public int getNounsCompleted() {
        return levelsCompleted[1];
    }

    public int getAdjCompleted() {
        return levelsCompleted[2];
    }

    public void incrementEngDictLevelsCompleted() {
        if(levelsCompleted[0] < 4) {
            levelsCompleted[0]++;
        }
    }

    public void incrementNounLevelsCompleted() {
        if(levelsCompleted[1] < 4) {
            levelsCompleted[1]++;
        }
    }

    public void incrementAdjectiveLevelsCompleted() {
        if(levelsCompleted[2] < 4) {
            levelsCompleted[2]++;
        }
    }

    public void addHighScore(int i, int levelSelected, int total) {
        switch (i) {
            case 0:
                if(total > engHighScores[levelSelected - 1]) {
                    engHighScores[levelSelected - 1] = total;
                }
                break;
            case 1:
                if(total > nounHighScores[levelSelected - 1]) {
                    nounHighScores[levelSelected - 1] = total;
                }
                break;
            case 2:
                if(total > adjHighScores[levelSelected - 1]) {
                    adjHighScores[levelSelected - 1] = total;
                }
                break;
            default:
                break;
        }
    }
}
