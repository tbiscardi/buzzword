package data;


import components.AppDataComponent;
import components.AppFileComponent;

import java.io.IOException;
import java.nio.file.Path;

public class GameFile implements AppFileComponent{

    @Override
    public void saveData(AppDataComponent data, Path to) {

    }

    @Override
    public void loadData(AppDataComponent data, Path from) throws IOException {

    }

    /** This method will be used if we need to export data into other formats. */
    @Override
    public void exportData(AppDataComponent data, Path filePath) throws IOException { }
}
