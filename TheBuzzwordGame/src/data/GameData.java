package data;


import apptemplate.AppTemplate;
import components.AppDataComponent;

public class GameData implements AppDataComponent{

    public AppTemplate appTemplate;
    private String[] letters;
    private char[][] letts;

    public GameData(AppTemplate appTemplate) {
        this.appTemplate = appTemplate;
        createLetterGrid();
    }

    private int randomWithRange(int min, int max) {
        int range = (max - min) + 1;
        return (int)(Math.random() * range) + min;
    }

    public char[][] getGrid() {
        return letts;
    }

    private void createLetterGrid() {
        letters = new String[16];
        for(int x = 0; x < 16; x ++) {
            String letter = "";
            int lett = randomWithRange(1, 10000);
            if(lett <= 1202) {
                letter = "E";
            } else if(lett >= 1203 && lett <= 2112) {
                letter = "T";
            } else if(lett >= 2113 && lett <= 2924) {
                letter = "A";
            } else if(lett >= 2925 && lett <= 3692) {
                letter = "O";
            } else if(lett >= 3693 && lett <= 4423) {
                letter = "I";
            } else if(lett >= 4424 && lett <= 5118) {
                letter = "N";
            } else if(lett >= 5119 && lett <= 5746) {
                letter = "S";
            } else if(lett >= 5747 && lett <= 6348) {
                letter = "R";
            } else if(lett >= 6349 && lett <= 6940) {
                letter = "H";
            } else if(lett >= 6941 && lett <= 7372) {
                letter = "D";
            } else if(lett >= 7373 && lett <= 7770) {
                letter = "L";
            } else if(lett >= 7771 && lett <= 8058) {
                letter = "U";
            } else if(lett >= 8059 && lett <= 8329) {
                letter = "C";
            } else if(lett >= 8330 && lett <= 8590) {
                letter = "M";
            } else if(lett >= 8591 && lett <= 8820) {
                letter = "F";
            } else if(lett >= 8821 && lett <= 9031) {
                letter = "Y";
            } else if(lett >= 9032 && lett <= 9240) {
                letter = "W";
            } else if(lett >= 9241 && lett <= 9443) {
                letter = "G";
            } else if(lett >= 9444 && lett <= 9625) {
                letter = "P";
            } else if(lett >= 9626 && lett <= 9774) {
                letter = "B";
            } else if(lett >= 9775 && lett <= 9885) {
                letter = "V";
            } else if(lett >= 9886 && lett <= 9954) {
                letter = "K";
            } else if(lett >= 9955 && lett <= 9971) {
                letter = "X";
            } else if(lett >= 9972 && lett <= 9982) {
                letter = "Q";
            } else if(lett >= 9983 && lett <= 9992) {
                letter = "J";
            } else if(lett >= 9993) {
                letter = "Z";
            }
            letters[x] = letter;
        }
        letts = new char[4][4];
        int z = 0;
        for(int x = 0; x < 4; x ++) {
            for(int y = 0; y < 4; y ++) {
                letts[x][y] = letters[z].charAt(0);
                z++;
            }
        }
    }

    @Override
    public void reset() {

    }
}
