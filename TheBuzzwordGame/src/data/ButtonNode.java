package data;

import javafx.scene.control.Button;

import java.util.ArrayList;

public class ButtonNode {

    private Button node;
    private int x;
    private int y;

    public ButtonNode(Button b, int x, int y) {
        this.node = b;
        this.x = x;
        this.y = y;
    }

    public Button getNode() {
        return node;
    }

    public void setNode(Button node) {
        this.node = node;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public ArrayList<Button> getNeighbors(Button[][] fields) {
        ArrayList<Button> neighbors = new ArrayList<>();
        try {
            neighbors.add(new ButtonNode(fields[x - 1][y - 1], x - 1, y - 1).getNode());
        } catch (Exception e) {

        }
        try {
            neighbors.add(new ButtonNode(fields[x][y - 1], x, y - 1).getNode());
        } catch (Exception e) {

        }
        try {
            neighbors.add(new ButtonNode(fields[x + 1][y - 1], x + 1, y - 1).getNode());
        } catch (Exception e) {

        }
        try {
            neighbors.add(new ButtonNode(fields[x - 1][y], x - 1, y).getNode());
        } catch (Exception e) {

        }
        try {
            neighbors.add(new ButtonNode(fields[x + 1][y], x + 1, y).getNode());
        } catch (Exception e) {

        }
        try {
            neighbors.add(new ButtonNode(fields[x - 1][y + 1], x - 1, y + 1).getNode());
        } catch (Exception e) {

        }
        try {
            neighbors.add(new ButtonNode(fields[x][y + 1], x, y + 1).getNode());
        } catch (Exception e) {

        }
        try {
            neighbors.add(new ButtonNode(fields[x + 1][y + 1], x + 1, y + 1).getNode());
        } catch (Exception e) {

        }




        return neighbors;
    }
}
