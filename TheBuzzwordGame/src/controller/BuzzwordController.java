package controller;

import apptemplate.AppTemplate;
import data.BuzzwordSolver;
import data.BuzzwordState;
import data.GameData;
import data.Player;
import gui.Workspace;
import propertymanager.PropertyManager;
import ui.ChangePasswordDialogSingleton;
import ui.CreateProfileDialogSingleton;
import ui.LoginProfileDialogSingleton;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.io.*;
import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.util.*;

import static settings.AppPropertyType.*;

public class BuzzwordController implements FileController {

    private AppTemplate appTemplate;
    private GameData gameData;
    private Workspace gameWorkspace;
    private BuzzwordState buzzwordState;
    private Player player;
    private BuzzwordSolver bs;
    private String gameMode;
    private String generatedSecuredPasswordHash;
    private static final NavigableSet<String> wordsDictionary = new TreeSet<>();
    private static final NavigableSet<String> nounsDictionary = new TreeSet<>();
    private static final NavigableSet<String> adjDictionary = new TreeSet<>();
    private String word;




    public BuzzwordController(AppTemplate appTemplate) {
        this.appTemplate = appTemplate;
        gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
        try {
            ClassLoader classLoader = getClass().getClassLoader();
            File file = new File(classLoader.getResource("words/words.txt").getFile());
            FileReader fr = new FileReader(file);
            BufferedReader br = new BufferedReader(fr);
            String line;
            while ((line = br.readLine()) != null) {
                wordsDictionary.add(line.toUpperCase());
            }
            File file2 = new File(classLoader.getResource("words/nouns.txt").getFile());
            FileReader fr2 = new FileReader(file2);
            BufferedReader br2 = new BufferedReader(fr2);
            String line2;
            while ((line2 = br2.readLine()) != null) {
                nounsDictionary.add(line2.toUpperCase());
            }
            File file3 = new File(classLoader.getResource("words/adjectives.txt").getFile());
            FileReader fr3 = new FileReader(file3);
            BufferedReader br3 = new BufferedReader(fr3);
            String line3;
            while ((line3 = br3.readLine()) != null) {
                adjDictionary.add(line3.toUpperCase());
            }
        } catch (Exception e) {
            throw new RuntimeException("Error while reading dictionary");
        }
//        buzzwordState = appTemplate.getWorkspaceComponent().getBuzzwordState();
    }




    @Override
    public void handleNewProfileRequest() throws IOException {
        buzzwordState = appTemplate.getWorkspaceComponent().getBuzzwordState();
        CreateProfileDialogSingleton createProfileDialogSingleton = CreateProfileDialogSingleton.getSingleton();
        createProfileDialogSingleton.show("CREATE NEW PROFILE");
        if(createProfileDialogSingleton.getSelection() == null) {
            buzzwordState.setState(0);
        } else {
            if (createProfileDialogSingleton.getSelection().equals("Okay")) {
                if(!createProfileDialogSingleton.getPasswordField().getText().equals(createProfileDialogSingleton.getConfirmField().getText())) {
                    createProfileDialogSingleton.getConfirmField().clear();
                    handleNewProfileRequest();
                    return;
                }
                File temp = new File("users.txt");
                if(!temp.exists()) {
                    try{
                        temp.createNewFile();
                    } catch (IOException e) {

                    }
                }
                Scanner file = new Scanner(temp);
                while(file.hasNextLine()) {
                    String input = file.nextLine();
                    if(input.equals(createProfileDialogSingleton.getUsernameField().getText())) {
                        createProfileDialogSingleton.resetSelection();
                        return;
                    } else {
                        file.nextLine();
                        file.nextLine();
                        file.nextLine();
                        file.nextLine();
                    }
                }
                FileWriter fw = new FileWriter(temp, true);
                BufferedWriter bw = new BufferedWriter(fw);
                PrintWriter pw = new PrintWriter(bw);
                pw.println(createProfileDialogSingleton.getUsernameField().getText());

                String originalPassword = createProfileDialogSingleton.getPasswordField().getText();
                generatedSecuredPasswordHash = null;
                try {
                    generatedSecuredPasswordHash = generateStrongPasswordHash(originalPassword);
                } catch (NoSuchAlgorithmException e) {

                } catch (InvalidKeySpecException e) {

                }
                pw.println(generatedSecuredPasswordHash);
                pw.println("0 0 0 0");
                pw.println("0 0 0 0");
                pw.println("0 0 0 0");
                pw.close();
                file.close();

//                buzzwordState.setState(1, loginProfileDialogSingleton.getUsernameField().getText());
                appTemplate.getWorkspaceComponent().reloadWorkspace();
            } else {
                buzzwordState.setState(0);
            }
        }
        createProfileDialogSingleton.resetSelection();
    }

    @Override
    public void handleLoginRequest() throws IOException {
        buzzwordState = appTemplate.getWorkspaceComponent().getBuzzwordState();
        LoginProfileDialogSingleton loginProfileDialogSingleton = LoginProfileDialogSingleton.getSingleton();
        loginProfileDialogSingleton.show("LOGIN");
        if(loginProfileDialogSingleton.getSelection() == null) {
            buzzwordState.setState(0);
        } else {
            if (loginProfileDialogSingleton.getSelection().equals("Okay")) {
                File temp = new File("users.txt");
                if(!temp.exists()) {
                    try{
                        temp.createNewFile();
                    } catch (IOException e) {

                    }
                    loginProfileDialogSingleton.resetSelection();
                    return;
                }
                Scanner file = new Scanner(temp);
                if(!file.hasNext()) {
                    loginProfileDialogSingleton.resetSelection();
                    return;
                }
                while(file.hasNextLine()) {
                    String user = file.nextLine();
                    String password = file.nextLine();

                    boolean matched = false;
                    try {
                        matched = validatePassword(loginProfileDialogSingleton.getPasswordField().getText(), password);
                    } catch (NoSuchAlgorithmException e) {

                    } catch (InvalidKeySpecException e) {

                    }

                    if(user.equals(loginProfileDialogSingleton.getUsernameField().getText()) && matched) {
                        player = new Player();

                        player.setUsername(user);
                        player.setPassword(generatedSecuredPasswordHash);

                        String[] engLevels = file.nextLine().split(" ");
                        int[] levelsArr = new int[4];
                        levelsArr[0] = Integer.parseInt(engLevels[0]);
                        levelsArr[1] = Integer.parseInt(engLevels[1]);
                        levelsArr[2] = Integer.parseInt(engLevels[2]);
                        levelsArr[3] = Integer.parseInt(engLevels[3]);

                        player.setEngHighScores(levelsArr);

                        String[] nounLevels = file.nextLine().split(" ");
                        int[] levelsArr2 = new int[4];
                        levelsArr2[0] = Integer.parseInt(nounLevels[0]);
                        levelsArr2[1] = Integer.parseInt(nounLevels[1]);
                        levelsArr2[2] = Integer.parseInt(nounLevels[2]);
                        levelsArr2[3] = Integer.parseInt(nounLevels[3]);

                        player.setNounHighScores(levelsArr2);

                        String[] adjLevels = file.nextLine().split(" ");
                        int[] levelsArr3 = new int[4];
                        levelsArr3[0] = Integer.parseInt(adjLevels[0]);
                        levelsArr3[1] = Integer.parseInt(adjLevels[1]);
                        levelsArr3[2] = Integer.parseInt(adjLevels[2]);
                        levelsArr3[3] = Integer.parseInt(adjLevels[3]);

                        player.setAdjHighScores(levelsArr3);

//                        player = new Player(user, generatedSecuredPasswordHash, levelsArr);

                        buzzwordState.setState(1, player);
                        appTemplate.getWorkspaceComponent().reloadWorkspace();
                    } else {
                        file.nextLine();
                        file.nextLine();
                        file.nextLine();
                    }
                }
                file.close();
            } else {
                buzzwordState.setState(0);
            }
        }
        loginProfileDialogSingleton.resetSelection();
    }


    @Override
    public void handleStartRequest() {
        try {
            buzzwordState.setState(2);
            appTemplate.getWorkspaceComponent().reloadWorkspace();
        } catch (NullPointerException e) {

        }
    }

    @Override
    public void handleLogoutRequest() {
        buzzwordState.setState(0);
        appTemplate.getWorkspaceComponent().reloadWorkspace();
    }

    @Override
    public void handleHomeRequest() {
        buzzwordState.setState(1);
        appTemplate.getWorkspaceComponent().reloadWorkspace();
    }

    @Override
    public void handleViewProfileRequest() {
        buzzwordState.setState(4);
        appTemplate.getWorkspaceComponent().reloadWorkspace();
    }

    public void start() {
        buzzwordState.setState(3);
        gameData = new GameData(appTemplate);

        PropertyManager props = PropertyManager.getManager();

        if(appTemplate.getGUI().getModeSelection().equals(props.getPropertyValue(ENGLISH_DICTIONARY_LABEL))) {
            gameMode = props.getPropertyValue(ENGLISH_DICTIONARY_LABEL);
            bs = new BuzzwordSolver(gameData.getGrid(), wordsDictionary);
            appTemplate.getWorkspaceComponent().reloadWorkspace();
            appTemplate.getWorkspaceComponent().populateButtons(gameData.getGrid());
//            appTemplate.getWorkspaceComponent().populateTable(bs.getList(), bs.getTotalScore());
        } else if(appTemplate.getGUI().getModeSelection().equals(props.getPropertyValue(NOUNS_LABEL))) {
            gameMode = props.getPropertyValue(NOUNS_LABEL);
            bs = new BuzzwordSolver(gameData.getGrid(), nounsDictionary);
            appTemplate.getWorkspaceComponent().reloadWorkspace();
            appTemplate.getWorkspaceComponent().populateButtons(gameData.getGrid());
//            appTemplate.getWorkspaceComponent().populateTable(bs.getList(), bs.getTotalScore());
        } else if(appTemplate.getGUI().getModeSelection().equals(props.getPropertyValue(ADJECTIVES_LABEL))){
            gameMode = props.getPropertyValue(ADJECTIVES_LABEL);
            bs = new BuzzwordSolver(gameData.getGrid(), adjDictionary);
            appTemplate.getWorkspaceComponent().reloadWorkspace();
            appTemplate.getWorkspaceComponent().populateButtons(gameData.getGrid());
//            appTemplate.getWorkspaceComponent().populateTable(bs.getList(), bs.getTotalScore());
        } else {
            gameMode = props.getPropertyValue(ENGLISH_DICTIONARY_LABEL);
            bs = new BuzzwordSolver(gameData.getGrid(), wordsDictionary);
            appTemplate.getWorkspaceComponent().reloadWorkspace();
            appTemplate.getWorkspaceComponent().populateButtons(gameData.getGrid());
//            appTemplate.getWorkspaceComponent().populateTable(bs.getList(), bs.getTotalScore());
        }
    }

    public void passwordChange() throws FileNotFoundException {
        ChangePasswordDialogSingleton changeDialog = ChangePasswordDialogSingleton.getSingleton();
        changeDialog.show("Password Change");
        if(changeDialog.getSelection() == null) {

        }

            if (changeDialog.getSelection().equals("Okay")) {
                if (!changeDialog.getPasswordField().getText().equals(changeDialog.getConfirmField().getText())) {
                    changeDialog.getConfirmField().clear();
                    passwordChange();
                    return;
                }


                File temp = new File("users.txt");
                Scanner file = new Scanner(temp);
                while(file.hasNextLine()) {
                    String user = file.nextLine();
                    if(player.getUsername().equals(user)) {
                        String passwordCheck = file.nextLine();
                        boolean matched = false;
                        try {
                            matched = validatePassword(changeDialog.getOldPasswordField().getText(), passwordCheck);
                        } catch (NoSuchAlgorithmException e) {

                        } catch (InvalidKeySpecException e) {

                        }
                        if(matched) {
                            String originalPassword = changeDialog.getPasswordField().getText();
                            generatedSecuredPasswordHash = null;
                            try {
                                generatedSecuredPasswordHash = generateStrongPasswordHash(originalPassword);
                            } catch (NoSuchAlgorithmException e) {

                            } catch (InvalidKeySpecException e) {

                            }
                            file = new Scanner(temp);
                            ArrayList<String> fileContent = new ArrayList<>();
                            while(file.hasNextLine()) {
                                String line = file.nextLine();
                                if(line.equals(player.getUsername())) {
                                    fileContent.add(line);
                                    fileContent.add(generatedSecuredPasswordHash);
                                    file.nextLine();
                                    fileContent.add(file.nextLine());
                                    fileContent.add(file.nextLine());
                                    fileContent.add(file.nextLine());
                                } else {
                                    fileContent.add(line);
                                }
                            }
                            PrintWriter writer = new PrintWriter(temp);
                            writer.print("");
                            for(int i = 0; i < fileContent.size(); i ++) {
                                writer.println(fileContent.get(i));
                            }
                            writer.close();
                        } else {
                            changeDialog.getOldPasswordField().clear();
                            changeDialog.getPasswordField().clear();
                            changeDialog.getConfirmField().clear();
                            passwordChange();
                            return;
                        }
                        break;
                    }
                }
            } else {

            }
            changeDialog.resetSelection();
        }

    public void resetProgress() {

    }


    public Player getPlayer() {
        return player;
    }

    public int getTotal() {
        return bs.getTotalScore();
    }

    public BuzzwordSolver getSolver() {
        return bs;
    }

    public boolean checkWord(String word) {
        PropertyManager props = PropertyManager.getManager();
        if(gameMode.equals(props.getPropertyValue(ENGLISH_DICTIONARY_LABEL))) {
            if(wordsDictionary.contains(word)) {
                return true;
            } else {
                return false;
            }
        } else if(gameMode.equals(props.getPropertyValue(NOUNS_LABEL))) {
            if(nounsDictionary.contains(word)) {
                return true;
            } else {
                return false;
            }
        } else {
            if(adjDictionary.contains(word)) {
                return true;
            } else {
                return false;
            }
        }
    }

    private static String generateStrongPasswordHash(String password) throws NoSuchAlgorithmException, InvalidKeySpecException
    {
        int iterations = 1000;
        char[] chars = password.toCharArray();
        byte[] salt = getSalt();
        PBEKeySpec spec = new PBEKeySpec(chars, salt, iterations, 64 * 8);
        SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
        byte[] hash = skf.generateSecret(spec).getEncoded();
        return iterations + ":" + toHex(salt) + ":" + toHex(hash);
    }

    private static byte[] getSalt() throws NoSuchAlgorithmException
    {
        SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
        byte[] salt = new byte[16];
        sr.nextBytes(salt);
        return salt;
    }


    private static String toHex(byte[] array) throws NoSuchAlgorithmException
    {
        BigInteger bi = new BigInteger(1, array);
        String hex = bi.toString(16);
        int paddingLength = (array.length * 2) - hex.length();
        if(paddingLength > 0)
        {
            return String.format("%0" +paddingLength + "d", 0) + hex;
        }else{
            return hex;
        }
    }

    private static boolean validatePassword(String originalPassword, String storedPassword) throws NoSuchAlgorithmException, InvalidKeySpecException
    {
        String[] parts = storedPassword.split(":");
        int iterations = Integer.parseInt(parts[0]);
        byte[] salt = fromHex(parts[1]);
        byte[] hash = fromHex(parts[2]);

        PBEKeySpec spec = new PBEKeySpec(originalPassword.toCharArray(), salt, iterations, hash.length * 8);
        SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
        byte[] testHash = skf.generateSecret(spec).getEncoded();

        int diff = hash.length ^ testHash.length;
        for(int i = 0; i < hash.length && i < testHash.length; i++)
        {
            diff |= hash[i] ^ testHash[i];
        }
        return diff == 0;
    }
    private static byte[] fromHex(String hex) throws NoSuchAlgorithmException
    {
        byte[] bytes = new byte[hex.length() / 2];
        for(int i = 0; i<bytes.length ;i++)
        {
            bytes[i] = (byte)Integer.parseInt(hex.substring(2 * i, 2 * i + 2), 16);
        }
        return bytes;
    }



}
