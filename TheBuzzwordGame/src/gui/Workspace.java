package gui;


import apptemplate.AppTemplate;
import components.AppWorkspaceComponent;
import controller.BuzzwordController;
import data.*;
import javafx.animation.AnimationTimer;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.util.Duration;
import propertymanager.PropertyManager;
import ui.AppGUI;
import ui.YesNoCancelDialogSingleton;

import java.io.*;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Scanner;

import static settings.AppPropertyType.*;


public class Workspace extends AppWorkspaceComponent{

    AppTemplate app; // the actual application
    AppGUI gui; // the GUI inside which the application sits
    BuzzwordState state;
    BuzzwordController controller;

    Label guiHeadingLabel, level, timeField, timeRemaining;
    Button pause, nextLevel, replay, changePassword, reset;
    HBox levelPausePane, timeBox;
    VBox headPane, gamePane;
    TextField  guessField, totalField, totalField2;
    TextArea   targetArea;
    TableView<Word> guessArea, allWordsTable;
    BorderPane root;
    GridPane   letters;
    Button[][] fields;
    Button[] levels;
    Image playImage, pauseImage;
    ImageView imageView;
    ObservableList<Word> guesses;
    private int levelSelected, total, index, target;
    private Timeline timeline;
    final static Integer STARTTIME = 60;
    private IntegerProperty timeSeconds = new SimpleIntegerProperty(STARTTIME);
    private String word;
    ArrayList<Button> wordParts;
    ArrayList<String> guesses2 = new ArrayList<>();
    private boolean success = false;
    AnimationTimer timer;
    ArrayList<ArrayList<ButtonNode>> allPossible;

    TableView<Score> scoreTableView;
    ObservableList<Score> scoreObservableList;

    final KeyCombination newProfComb = new KeyCodeCombination(KeyCode.P, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN);
    final KeyCombination loginComb = new KeyCodeCombination(KeyCode.L, KeyCombination.CONTROL_DOWN);
    final KeyCombination startPlayComb = new KeyCodeCombination(KeyCode.P, KeyCombination.CONTROL_DOWN);
    final KeyCombination restartComb = new KeyCodeCombination(KeyCode.R, KeyCombination.CONTROL_DOWN);
    final KeyCombination nextLevelComb = new KeyCodeCombination(KeyCode.PERIOD, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN);


    public Workspace(AppTemplate initApp) throws IOException {
        app = initApp;
        gui = app.getGUI();

        layoutGUI();     // initialize all the workspace (GUI) components including the containers and their layout
        setupHandlers(); // ... and set up event handling
        state = new BuzzwordState(0);
    }

    private void layoutGUI() {
        PropertyManager propertyManager = PropertyManager.getManager();
        guiHeadingLabel = new Label(propertyManager.getPropertyValue(HEADING_LABEL));
        guiHeadingLabel.setTextFill(Color.WHITE);
        guiHeadingLabel.setFont(new Font(30));
        guiHeadingLabel.setAlignment(Pos.CENTER_LEFT);

        headPane = new VBox();
        headPane.setPadding(new Insets(10, 0, 70, 0));
        headPane.getChildren().add(guiHeadingLabel);


        root = new BorderPane();
//        root.setTop(headPane);

        fields = new Button[4][4];

        letters = new GridPane();
        letters.setHgap(30);
        letters.setVgap(30);
        letters.setPadding(new Insets(70, 0, 70, 0));
        letters.setAlignment(Pos.CENTER);
        for(int x = 0; x < 4; x++) {
            for(int y = 0; y < 4; y++) {
                Circle c = new Circle(28);
                Button letter = new Button();
                letter.setShape(c);
                letter.setDisable(false);
                letter.setVisible(true);
                letter.setPrefSize(56, 56);
                letter.setStyle("-fx-text-fill: white; -fx-text-inner-color: white; -fx-background-color: #373737;");
                letter.setAlignment(Pos.CENTER);
                fields[x][y] = letter;
                letters.setConstraints(letter, x, y);
                letters.getChildren().add(letter);
            }
        }

        fields[0][0].setText("B");
        fields[1][0].setText("U");
        fields[0][1].setText("Z");
        fields[1][1].setText("Z");
        fields[2][2].setText("W");
        fields[3][2].setText("O");
        fields[2][3].setText("R");
        fields[3][3].setText("D");

        levels = new Button[4];



        headPane.getChildren().add(letters);

        root.setTop(headPane);
        headPane.setAlignment(Pos.CENTER);
        workspace = root;
    }

    private void setupHandlers() {
        controller = (BuzzwordController ) app.getGUI().getFileController();


    }

    @Override
    public void initStyle() {

    }

    @Override
    public void reloadWorkspace() {
        switch (state.getState()) {
            case 0:
                homeLoginPage();
                break;
            case 1:
                homePage();
                break;
            case 2:
                levelSelectionPage();
                break;
            case 3:
                gameplayPage();
                break;
            case 4:
                profilePage();
            default:
                layoutGUI();
                break;
        }

    }

    private void profilePage() {
        root = new BorderPane();
        workspaceActivated = false;
        profilePageGUI();
        activateWorkspace(gui.getAppPane());
        gui.getWindow().setOnCloseRequest(e -> System.exit(0));
        gui.updateWorkspaceToolbar(3);
    }

    private void homeLoginPage() {
        root = new BorderPane();
        workspaceActivated = false;
        layoutGUI();
        activateWorkspace(gui.getAppPane());
        gui.getWindow().setOnCloseRequest(e -> System.exit(0));
        gui.updateWorkspaceToolbar(0);
    }

    private void homePage() {
        root = new BorderPane();
        workspaceActivated = false;
        layoutGUI();
        activateWorkspace(gui.getAppPane());
        gui.getWindow().setOnCloseRequest(e -> System.exit(0));
        gui.updateWorkspaceToolbar(1);
    }

    private void levelSelectionPage() {
        root = new BorderPane();
        workspaceActivated = false;
        levelSelecionGUI();
        activateWorkspace(gui.getAppPane());
        gui.updateWorkspaceToolbar(2);
        gui.getWindow().setOnCloseRequest(e -> System.exit(0));
        levels[0].setOnAction(e -> {
            levelSelected = 1;
            controller.start();
        });
        levels[1].setOnAction(e -> {
            levelSelected = 2;
            controller.start();
        });
        levels[2].setOnAction(e -> {
            levelSelected = 3;
            controller.start();
        });
        levels[3].setOnAction(e -> {
            levelSelected = 4;
            controller.start();
        });
    }

    private void gameplayPage() {
        root = new BorderPane();
        workspaceActivated = false;
        gameplayGUI();
        activateWorkspace(gui.getAppPane());
        gui.updateWorkspaceToolbar(2);
        gui.getWindow().setOnCloseRequest(new EventHandler<WindowEvent>() {
            public void handle(WindowEvent ev) {
                timeline.pause();
                letters.setVisible(false);
                imageView.setImage(playImage);
                YesNoCancelDialogSingleton dialog = YesNoCancelDialogSingleton.getSingleton();
                dialog.show("Exit", "Would you like the exit?");
                if (dialog.getSelection() == null) {
                    letters.setVisible(true);
                    imageView.setImage(pauseImage);
                    timeline.play();
                    ev.consume();
                }
                String selection = dialog.getSelection();
                if (selection.equals("Yes")) {
                    System.exit(0);
                } else {
                    letters.setVisible(true);
                    imageView.setImage(pauseImage);
                    timeline.play();
                    ev.consume();
                }
            }
        });



//        timer = new AnimationTimer() {
//            private long lastUpdate = 0;
//            @Override
//            public synchronized void handle(long now) {
//
//                gui.getPrimaryScene().setOnKeyTyped((KeyEvent event) -> {
//                    char typed = event.getCharacter().toUpperCase().charAt(0);
//                    if (Character.isLetter(typed)) {
//                        if(word == null) {
//                            word = "";
//                        }
//                        int wordSize = word.length();
//                        int index2 = 0;
//                        if(word.length() < 1) {
//                            allPossible = new ArrayList<>();
//
//
//                            for(int x = 0; x < 4; x++) {
//                                for(int y = 0; y < 4; y++) {
//                                    if(fields[x][y].getText().charAt(0) == typed) {
//                                        allPossible.add(new ArrayList<>());
//                                        allPossible.get(index2).add(new ButtonNode(fields[x][y], x, y));
//                                        index2++;
//                                        fields[x][y].setStyle("-fx-text-fill: black; -fx-text-inner-color: black; -fx-background-color: white;");
//                                        if(word.length() < 1) {
//                                            word = word + typed;
//                                        }
//                                    }
//                                }
//                            }
//                        }
//                        else if(word.length() > 0) {
//                            timer.stop();
//                            ArrayList<ButtonNode> temp = new ArrayList<>();
//                            for(int x = 0; x < 4; x++) {
//                                for (int y = 0; y < 4; y++) {
//                                    if(fields[x][y].getText().charAt(0) == typed) {
//                                        temp.add(new ButtonNode(fields[x][y], x, y));
//                                    }
//                                }
//                            }
//                            int branch = 0;
//                            int y = 0;
//                            for(int x = 0; x < allPossible.size(); x ++) {
//                                for(y = 0; y < temp.size(); y ++) {
//                                    if(branch == 0) {
//                                        if (allPossible.get(x).get(wordSize-1).getNeighbors(fields).contains(temp.get(y).getNode())) {
//                                            allPossible.get(x).add(temp.get(y));
//                                            branch++;
//                                            if(word.length() == wordSize) {
//                                                word = word + typed;
//                                            }
//                                        }
//                                    } else {
//                                        if (allPossible.get(x).get(wordSize - 1).getNeighbors(fields).contains(temp.get(y).getNode())) {
//                                            ArrayList<ButtonNode> newList = new ArrayList<>();
//                                            for(int i = 0; i < allPossible.get(x).size(); i ++) {
//                                                newList.add(allPossible.get(x).get(i));
//                                            }
//                                            allPossible.add(newList);
//                                            allPossible.get(allPossible.size()-1).remove(wordSize);
//                                            allPossible.get(allPossible.size()-1).add(temp.get(y));
//                                            branch++;
//                                        }
//                                    }
//
//                                }
//                                if(branch == 0) {
//                                    allPossible.remove(x);
//                                    x--;
//                                } else {
//
//                                }
//                                branch = 0;
//                            }
//                            boolean flag = false;
//                            for(int i = 0; i < 4; i ++) {
//                                for(int j = 0; j < 4; j++) {
//                                    for(int k = 0; k < allPossible.size(); k++) {
//                                        for(int l = 0; l < allPossible.get(k).size(); l++) {
//                                            if(allPossible.get(k).get(l).getNode().equals(fields[i][j])) {
//                                                fields[i][j].setStyle("-fx-text-fill: black; -fx-text-inner-color: black; -fx-background-color: white;");
//                                                flag = true;
//                                            }
//                                        }
//                                    }
//                                    if(!flag) {
//                                        fields[i][j].setStyle("-fx-text-fill: white; -fx-text-inner-color: white; -fx-background-color: #373737;");
//                                    }
//                                }
//                            }
//                        }
//
//                    }
//
//                });
//                gui.getPrimaryScene().setOnKeyPressed((KeyEvent event2) -> {
//                    if(event2.getCode().equals(KeyCode.ENTER)) {
//                        System.out.println(word);
//                        word = "";
//                    }
//                });
//            }
//        };
//        timer.start();

    }

    private void profilePageGUI() {
        PropertyManager propertyManager = PropertyManager.getManager();
        guiHeadingLabel = new Label("Profile");
        guiHeadingLabel.setTextFill(Color.WHITE);
        guiHeadingLabel.setFont(new Font(40));
        guiHeadingLabel.setAlignment(Pos.CENTER);

        Label usernameLabel = new Label("Username: " + controller.getPlayer().getUsername());
        usernameLabel.setTextFill(Color.WHITE);
        usernameLabel.setFont(new Font(24));
        changePassword = new Button("Change Password");
        changePassword.setOnAction(e -> {
            try {
                controller.passwordChange();
            } catch (FileNotFoundException e1) {

            }
        });
        reset = new Button("Reset Progress");
        reset.setOnAction(e -> controller.resetProgress());
        //personal bests? tableview most likely
        scoreTableView = new TableView<>();
        TableColumn<Score, Integer> levelCol = new TableColumn<>("Level");
        levelCol.setMinWidth(100);
        levelCol.setCellValueFactory(new PropertyValueFactory<Score, Integer>("level"));

        TableColumn<Score, Integer> score1Col = new TableColumn<>("Dictionary");
        score1Col.setMinWidth(100);
        score1Col.setCellValueFactory(new PropertyValueFactory<Score, Integer>("score1"));

        TableColumn<Score, Integer> score2Col = new TableColumn<>("Nouns");
        score2Col.setMinWidth(100);
        score2Col.setCellValueFactory(new PropertyValueFactory<Score, Integer>("score2"));

        TableColumn<Score, Integer> score3Col = new TableColumn<>("Adjectives");
        score3Col.setMinWidth(100);
        score3Col.setCellValueFactory(new PropertyValueFactory<Score, Integer>("score3"));

        scoreTableView.getColumns().addAll(levelCol, score1Col, score2Col, score3Col);

        scoreObservableList = FXCollections.observableArrayList();
        scoreObservableList.add(0, new Score(1, controller.getPlayer().getEngHighScores()[0], controller.getPlayer().getNounHighScores()[0], controller.getPlayer().getAdjHighScores()[0]));
        scoreObservableList.add(1, new Score(2, controller.getPlayer().getEngHighScores()[1], controller.getPlayer().getNounHighScores()[1], controller.getPlayer().getAdjHighScores()[1]));
        scoreObservableList.add(2, new Score(3, controller.getPlayer().getEngHighScores()[2], controller.getPlayer().getNounHighScores()[2], controller.getPlayer().getAdjHighScores()[2]));
        scoreObservableList.add(3, new Score(4, controller.getPlayer().getEngHighScores()[3], controller.getPlayer().getNounHighScores()[3], controller.getPlayer().getAdjHighScores()[3]));

        scoreTableView.setItems(scoreObservableList);
        scoreTableView.setMaxSize(410, 120);

        Label hsLabel = new Label("High Scores");
        hsLabel.setTextFill(Color.WHITE);
        hsLabel.setFont(new Font(20));

        headPane = new VBox(20);
        headPane.setPadding(new Insets(30, 0, 0, 60));
        headPane.getChildren().addAll(guiHeadingLabel, usernameLabel, changePassword, hsLabel, scoreTableView);
        root.setTop(headPane);
        workspace = root;

    }

    private void levelSelecionGUI() throws NullPointerException{
        PropertyManager propertyManager = PropertyManager.getManager();
        guiHeadingLabel = new Label(propertyManager.getPropertyValue(HEADING_LABEL));
        guiHeadingLabel.setTextFill(Color.WHITE);
        guiHeadingLabel.setFont(new Font(30));
        guiHeadingLabel.setAlignment(Pos.CENTER);

        level = new Label(gui.getModeSelection());
        level.setTextFill(Color.WHITE);
        level.setFont(new Font(30));
        level.setAlignment(Pos.CENTER);
        level.setPadding(new Insets(10, 0, 0, 0));

        headPane = new VBox();
        headPane.setPadding(new Insets(10, 0, 80, 0));
        headPane.getChildren().addAll(guiHeadingLabel, level);
        headPane.setAlignment(Pos.CENTER);

        root = new BorderPane();


        levels = new Button[4];

        letters = new GridPane();
        letters.setHgap(30);
        letters.setVgap(30);
        letters.setPadding(new Insets(30, 0, 70, 0));
        letters.setAlignment(Pos.CENTER);


        int[] levelArr = controller.getPlayer().getLevelsCompleted();
        int z = 0;
        if (level.getText().equals(propertyManager.getPropertyValue(ENGLISH_DICTIONARY_LABEL))) {
            z = 0;
        } else if (level.getText().equals(propertyManager.getPropertyValue(NOUNS_LABEL))) {
            z = 1;
        } else if (level.getText().equals(propertyManager.getPropertyValue(ADJECTIVES_LABEL))){
            z = 2;
        } else {
            z = 0;
        }
        int levelS = levelArr[z];
        for (int x = 1; x <= 4; x++) {
            Circle c = new Circle(28);
            Button letter = new Button("" + x);
            letter.setShape(c);
            letter.setDisable(false);
            letter.setVisible(true);
            letter.setPrefSize(56, 56);
            if (x <= levelS + 1) {
                letter.setStyle("-fx-text-fill: black; -fx-text-inner-color: black; -fx-background-color: white;");
                letter.setDisable(false);
            } else {
                letter.setStyle("-fx-text-fill: white; -fx-text-inner-color: white; -fx-background-color: #373737;");
                letter.setDisable(true);
            }
//            if(y == 1) {
//                letter.setStyle("-fx-text-fill: white; -fx-text-inner-color: white; -fx-background-color: #373737;");
//                letter.setDisable(true);
//            } else {
//                letter.setStyle("-fx-text-fill: black; -fx-text-inner-color: black; -fx-background-color: white;");
//                letter.setDisable(false);
//            }
                letter.setAlignment(Pos.CENTER);
                levels[x - 1] = letter;
                letters.setConstraints(letter, x, 0);
                letters.getChildren().add(letter);
            }



            headPane.getChildren().add(letters);
            root.setTop(headPane);
            workspace = root;
    }

    private void gameplayGUI() {
        PropertyManager propertyManager = PropertyManager.getManager();
        guiHeadingLabel = new Label(propertyManager.getPropertyValue(HEADING_LABEL));
        guiHeadingLabel.setTextFill(Color.WHITE);
        guiHeadingLabel.setFont(new Font(30));
        guiHeadingLabel.setAlignment(Pos.CENTER_LEFT);


        try {
            level = new Label(gui.getModeSelection());
        } catch (NullPointerException e) {

        }
        level.setTextFill(Color.WHITE);
        level.setFont(new Font(30));
        level.setAlignment(Pos.CENTER);
        level.setPadding(new Insets(10, 0, 0 ,0));
        headPane = new VBox();
        headPane.setPadding(new Insets(10, 0, 80, 0));
        headPane.getChildren().addAll(guiHeadingLabel, level);


        root = new BorderPane();
//        root.setTop(headPane);

        fields = new Button[4][4];

        letters = new GridPane();
        letters.setHgap(30);
        letters.setVgap(30);
        letters.setPadding(new Insets(30, 0, 70, 0));
        letters.setAlignment(Pos.CENTER);
        for(int x = 0; x < 4; x++) {
            for(int y = 0; y < 4; y++) {
                Circle c = new Circle(28);
                Button letter = new Button();
                letter.setShape(c);
                letter.setVisible(true);
                letter.setPrefSize(56, 56);
                letter.setStyle("-fx-text-fill: white; -fx-text-inner-color: white; -fx-background-color: #373737;");
                letter.setAlignment(Pos.CENTER);
                fields[x][y] = letter;
                fields[x][y].setOnDragDetected(e -> {
                    dragWordStartFunction(letter);
                });
                fields[x][y].setOnMouseDragEntered(e -> {
                    draggedOverFunction(letter);
                });
                fields[x][y].setOnMouseDragReleased(e -> {
                    gui.getPrimaryScene().setOnMouseDragExited(e1 -> {

                    });
                    dragWordEndFunction();
                });
                letters.setConstraints(letter, x, y);
                letters.getChildren().add(letter);
            }
        }

        levels = new Button[4];

        headPane.getChildren().add(letters);

        headPane.setAlignment(Pos.CENTER);

        gamePane = new VBox();
        play();

        timeField = new Label("Time Remaining: ");
        timeField.setVisible(true);
        timeField.setAlignment(Pos.CENTER);
        timeField.setStyle("-fx-text-fill: red; -fx-background-color: transparent;");
        timeRemaining = new Label();
        timeRemaining.setStyle("-fx-text-fill: red; -fx-background-color: transparent;");
        timeRemaining.textProperty().bind(timeSeconds.asString());

        timeBox = new HBox();
        timeBox.setAlignment(Pos.CENTER);
        timeBox.getChildren().addAll(timeField, timeRemaining);



        guessField = new TextField();
        guessField.setPromptText("Type word here");
        guessField.setVisible(true);
        guessField.setShape(new Rectangle(160, 30));
        guessField.setPrefWidth(160);
        guessField.setMaxWidth(160);
        guessField.setStyle("-fx-text-fill: white; -fx-background-color: gray;");

        guessField.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent keyEvent) {
                if (keyEvent.getCode() == KeyCode.ENTER)  {
                    String word2 = guessField.getText().toUpperCase();
                    if(word2.length() > 2) {
                        if (controller.checkWord(word2)) {
                            if(guesses2.contains(word2)){

                            } else {
                                Word newWord = new Word(word2);
                                guesses.add(newWord);
                                guesses2.add(word2);
                                total += newWord.getScore();
                                totalField.setText("TOTAL                   " + total);
                            }
                        }
                    } else {

                    }





                    guessField.setText("");
                }
            }
        });


        guessArea = new TableView<>();
        TableColumn<Word, String> wordCol = new TableColumn<>("Word");
        wordCol.setMinWidth(100);
        wordCol.setCellValueFactory(new PropertyValueFactory<>("word"));

        TableColumn<Word, String> scoreCol = new TableColumn<>("Score");
        scoreCol.setMinWidth(40);
        scoreCol.setCellValueFactory(new PropertyValueFactory<>("score"));

        guessArea.setMaxSize(160, 250);

        guessArea.getColumns().addAll(wordCol, scoreCol);
        guesses = FXCollections.observableArrayList();
        guessArea.setItems(guesses);




        totalField = new TextField();
        totalField.setEditable(false);
        totalField.setDisable(false);
        totalField.setMouseTransparent(true);
        totalField.setFocusTraversable(false);
        totalField.setVisible(true);
        totalField.setShape(new Rectangle(160, 30));
        totalField.setPrefWidth(160);
        totalField.setMaxWidth(160);
        totalField.setStyle("-fx-text-fill: white; -fx-background-color: grey;");
        gamePane.setAlignment(Pos.CENTER_RIGHT);

        targetArea = new TextArea();
        targetArea.setEditable(false);
        targetArea.setDisable(false);
        targetArea.setMouseTransparent(true);
        targetArea.setFocusTraversable(false);
        targetArea.setVisible(true);
        targetArea.setPrefSize(160, 70);
        targetArea.setMaxSize(160, 70);

        total = 0;
        targetArea.setText("Target");
        totalField.setText("TOTAL                   " + total);
        nextLevel = new Button("Next Level");
        nextLevel.setVisible(false);
        nextLevel.setOnAction(e -> {

        });

        replay = new Button("Replay Level");
        replay.setOnAction(e -> {
            controller.start();
        });


        playImage = new Image(getClass().getResourceAsStream("/images/play.png"));
        pauseImage = new Image(getClass().getResourceAsStream("/images/pause.png"));
        imageView = new ImageView(pauseImage);
        imageView.setFitHeight(50);
        imageView.setFitWidth(50);
        pause = new Button();
        pause.setGraphic(imageView);
        pause.setMaxSize(50, 50);
        pause.setOnAction(e -> {
            if(imageView.getImage().equals(pauseImage)) {
                timeline.pause();
                letters.setVisible(false);
                imageView.setImage(playImage);
            } else {
                timeline.play();
                letters.setVisible(true);
                imageView.setImage(pauseImage);
            }
        });
        pause.setStyle("-fx-background-color: transparent; -fx-text-fill: white;");
        pause.setVisible(true);

        level = new Label("Level " + levelSelected);
        level.setFont(new Font(25));
        level.setStyle("-fx-text-fill: white");
        levelPausePane = new HBox(20);

        levelPausePane.getChildren().addAll(level, pause);
        levelPausePane.setAlignment(Pos.CENTER);

        headPane.getChildren().add(levelPausePane);
        gamePane.getChildren().addAll(timeBox, guessField, guessArea, totalField, targetArea, nextLevel, replay);
        HBox pan = new HBox(50);
        pan.getChildren().addAll(headPane, gamePane);
        root.setTop(pan);
        workspace = root;


        generateTargetScore(controller.getTotal() , levelSelected);

    }

    public void populateButtons(char[][] letters) {
        success = false;
        for(int x = 0; x < 4; x++) {
            for (int y = 0; y < 4; y++) {
                fields[x][y].setText(String.valueOf(letters[x][y]));
            }
        }
    }

    public void populateTable(ObservableList<Word> words, int total) {
        allWordsTable.setItems(words);
//        targetArea2.setText("Target");
        totalField2.setText("TOTAL                   " + total);
//        generateTargetScore(total, levelSelected);
    }

    private void generateTargetScore(int total, int levelSelected) {
        if (total < 200) {
            controller.start();
            return;
        }
        switch (levelSelected) {
            case 1:
                target = 40;
                targetArea.setText("TARGET SCORE\n\n" + target + " POINTS");
                break;
            case 2:
                target = 60;
                targetArea.setText("TARGET SCORE\n\n" + target + " POINTS");
                break;
            case 3:
                target = 80;
                targetArea.setText("TARGET SCORE\n\n" + target + " POINTS");
                break;
            case 4:
                target = 100;
                targetArea.setText("TARGET SCORE\n\n" + target + " POINTS");
                break;
            default:
                break;
        }
    }

    public void play(){
        if (timeline != null) {
            timeline.stop();
        }
        timeSeconds.set(STARTTIME);
        timeline = new Timeline();
        timeline.getKeyFrames().add(
                new KeyFrame(Duration.seconds(STARTTIME+1),
                        new KeyValue(timeSeconds, 0)));
        timeline.playFromStart();
        timeline.setOnFinished(e -> {
            try {
                endGame();
            } catch (FileNotFoundException e1) {

            }
        });
    }

    public void endGame() throws FileNotFoundException{
        for(int x = 0; x < 4; x++) {
            for(int y = 0; y < 4; y++) {
                fields[x][y].setOnDragDetected(e -> {

                });
                fields[x][y].setOnMouseDragEntered(e -> {

                });
                fields[x][y].setOnMouseDragReleased(e -> {

                });
            }
        }

        gui.getWindow().setOnCloseRequest(e -> System.exit(0));

        if(total >= target) {
            PropertyManager props = PropertyManager.getManager();
            success = true;
            if(gui.getModeSelection().equals(props.getPropertyValue(ENGLISH_DICTIONARY_LABEL))) {
                if(controller.getPlayer().getEnglishCompleted() == levelSelected - 1) {
                    controller.getPlayer().incrementEngDictLevelsCompleted();
                }
            } else if(gui.getModeSelection().equals(props.getPropertyValue(NOUNS_LABEL))) {
                if(controller.getPlayer().getNounsCompleted() == levelSelected - 1) {
                    controller.getPlayer().incrementNounLevelsCompleted();
                }
            } else {
                if(controller.getPlayer().getAdjCompleted() == levelSelected - 1) {
                    controller.getPlayer().incrementAdjectiveLevelsCompleted();
                }
            }
            if(success) {
                if(gui.getModeSelection().equals(props.getPropertyValue(ENGLISH_DICTIONARY_LABEL))) {
                    controller.getPlayer().addHighScore(0, levelSelected, total);
                } else if (gui.getModeSelection().equals(props.getPropertyValue(NOUNS_LABEL))) {
                    controller.getPlayer().addHighScore(1, levelSelected, total);
                } else {
                    controller.getPlayer().addHighScore(2, levelSelected, total);
                }
            }


            File temp = null;
            try {
                temp = new File("users.txt");
                Scanner file = new Scanner(temp);
                ArrayList<String> fileContent = new ArrayList<>();
                while(file.hasNextLine()) {
                        String line = file.nextLine();
                    if(line.equals(controller.getPlayer().getUsername())) {
                        fileContent.add(line);
                        fileContent.add(file.nextLine());
                        fileContent.add(controller.getPlayer().getEngHighScores()[0] + " " + controller.getPlayer().getEngHighScores()[1] + " " +
                                controller.getPlayer().getEngHighScores()[2] + " " + controller.getPlayer().getEngHighScores()[3]);
                        fileContent.add(controller.getPlayer().getNounHighScores()[0] + " " + controller.getPlayer().getNounHighScores()[1] + " " +
                                controller.getPlayer().getNounHighScores()[2] + " " + controller.getPlayer().getNounHighScores()[3]);
                        fileContent.add(controller.getPlayer().getAdjHighScores()[0] + " " + controller.getPlayer().getAdjHighScores()[1] + " " +
                                controller.getPlayer().getAdjHighScores()[2] + " " + controller.getPlayer().getAdjHighScores()[3]);
                        file.nextLine();
                        file.nextLine();
                        file.nextLine();
                    }
                    else {
                        fileContent.add(line);
                    }
                }
                PrintWriter writer = new PrintWriter(temp);
                writer.print("");
                for(int i = 0; i < fileContent.size(); i ++) {
                    writer.println(fileContent.get(i));
                }
                writer.close();
            } catch (IOException e) {

            }

            guesses2.clear();

            Stage newStage = new Stage();
            newStage.setTitle("All Words");
            VBox pane2 = new VBox(10);

            allWordsTable = new TableView<>();
            TableColumn<Word, String> wordCol = new TableColumn<>("Word");
            wordCol.setMinWidth(100);
            wordCol.setCellValueFactory(new PropertyValueFactory<>("word"));

            TableColumn<Word, String> scoreCol = new TableColumn<>("Score");
            scoreCol.setMinWidth(40);
            scoreCol.setCellValueFactory(new PropertyValueFactory<>("score"));

//            allWordsTable.setMaxSize(160, 250);

            allWordsTable.getColumns().addAll(wordCol, scoreCol);

            totalField2 = new TextField();
            totalField2.setEditable(false);
            totalField2.setDisable(false);
            totalField2.setMouseTransparent(true);
            totalField2.setFocusTraversable(false);
            totalField2.setVisible(true);
            totalField2.setShape(new Rectangle(allWordsTable.getWidth(), 30));
            totalField2.setPrefWidth(200);

            populateTable(controller.getSolver().getList(), controller.getSolver().getTotalScore());
            pane2.getChildren().addAll(allWordsTable, totalField2);


            Scene newScene = new Scene(pane2, 250, 300);
            pane2.prefWidthProperty().bind(newScene.widthProperty());
            pane2.prefHeightProperty().bind(newScene.heightProperty());
            newStage.setScene(newScene);
            newStage.show();


            if(levelSelected < 4) {
                nextLevel.setVisible(true);
                nextLevel.setOnAction(e -> {
                    levelSelected++;
                    controller.start();
                });
            }
            pause.setVisible(false);
            gui.getWindow().setOnCloseRequest(e -> {
                System.exit(0);
            });

        } else {
            success = false;
            nextLevel.setVisible(false);
        }
    }


    public void dragWordStartFunction(Button b) {
        index = 0;
        wordParts = new ArrayList<>();
        wordParts.add(b);
        wordParts.get(0).setStyle("-fx-text-fill: black; -fx-text-inner-color: black; -fx-background-color: white;");
        index++;
        gui.getPrimaryScene().setOnMouseDragExited(e -> {
            dragWordEndFunction();
        });
        word = wordParts.get(0).getText();
        wordParts.get(0).setMouseTransparent(true);
        wordParts.get(0).startFullDrag();
    }

    public void draggedOverFunction(Button b) {
        for(int i = 0; i < wordParts.size(); i ++) {
            if(wordParts.get(i) == null) {
                break;
            }
            if(wordParts.get(i).equals(b)) {
                return;
            }
        }
        int x ;
        int y = 0;
        boolean flag = true;
        for(x = 0; x < 4; x++) {
            for(y = 0; y < 4; y ++) {
                if(fields[x][y].equals(wordParts.get(wordParts.size()-1))) {
                    flag = false;
                    break;
                }
            }
            if(!flag) {
                break;
            }
        }
        ArrayList<Button> possible = new ArrayList<>();
        try {
            possible.add(fields[x - 1][y - 1]);
        } catch (Exception e) {

        }
        try {
            possible.add(fields[x][y - 1]);
        } catch (Exception e) {

        }
        try {
            possible.add(fields[x + 1][y - 1]);
        } catch (Exception e) {

        }
        try {
             possible.add(fields[x - 1][y]);
        } catch (Exception e) {

        }
        try {
            possible.add(fields[x + 1][y]);
        } catch (Exception e) {

        }
        try {
            possible.add(fields[x - 1][y + 1]);
        } catch (Exception e) {

        }
        try {
            possible.add(fields[x][y + 1]);
        } catch (Exception e) {

        }
        try {
            possible.add(fields[x + 1][y + 1]);
        } catch (Exception e) {

        }

        if(possible.contains(b)) {
            word += b.getText();
            wordParts.add(b);
            wordParts.get(index).setStyle("-fx-text-fill: black; -fx-text-inner-color: black; -fx-background-color: white;");
            index++;
        } else {

        }

    }

    public void dragWordEndFunction() {
        wordParts.get(0).setMouseTransparent(false);
        for(int i = 0; i < wordParts.size(); i ++) {
            if(wordParts.get(i) == null) {
                break;
            }
            wordParts.get(i).setStyle("-fx-text-fill: white; -fx-text-inner-color: white; -fx-background-color: #373737;");
        }

        if(word.length() < 3) {
            return;
        }
        if (controller.checkWord(word)) {
            if(guesses2.contains(word)){

            } else {
                Word newWord = new Word(word);
                guesses.add(newWord);
                guesses2.add(word);
                total += newWord.getScore();
                totalField.setText("TOTAL                   " + total);
//                if(total >= target) {
//                    if(levelSelected < 4) {
//                        nextLevel.setVisible(true);
//                        nextLevel.setOnAction(e -> {
//                            guesses2.clear();
//                            levelSelected++;
//                            controller.start();
//                        });
//                    }
//                }
            }
        }

        word = "";
    }



    @Override
    public BuzzwordState getBuzzwordState() {
        return this.state;
    }
}
